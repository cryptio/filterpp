/**
 * FilterPP
 * File: main.cpp
 * Purpose: Handles the necessary tasks for program functionality
 *
 * @author Encryptio
 * @version 0.12
 */

#include <dirent.h>
#include <iterator>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <string>
#include <sys/stat.h>
#include <syslog.h>
#include <unistd.h>
#include <vector>
#include "../include/filterpp.h"
#include "../include/abuseipdb.h"
#include "../include/exceptions.h"

FilterPP *FILTERPP = &FilterPP::get_instance();

void do_heartbeat() {
    std::unordered_map<std::string, int> fail_results;
    std::vector<unsigned long> ports;

    for (const auto &protocol : FILTERPP->get_protocols()) {
        // only keep unique IP address from each service
        const std::string *log_file = &protocol.get_log_file();
        const std::regex *expression = &protocol.get_expression();
        for (const auto &ip : FILTERPP->get_fails(*log_file, *expression)) {
            if (fail_results.find(ip.first) == fail_results.end())
                fail_results[ip.first] = ip.second;
        }
        // add all of the unique ports to a vector so we can block the IP addresses on those ports
        if (std::find(ports.begin(), ports.end(), protocol.get_port()) == ports.end())
            ports.push_back(protocol.get_port());
    }
    if (!fail_results.empty()) {
        std::vector <std::string> blacklisted_ips = FILTERPP->write_blacklist(fail_results,
                                                                              FILTERPP->get_blacklist());
        if (blacklisted_ips.empty()) {
            FILTERPP->log(LogLevel::INFO, "No IPs addresses blacklisted");
        } else {
            std::stringstream sb;
            sb << "Blacklisted IP addresses: \n";

            std::copy(blacklisted_ips.begin(), blacklisted_ips.end() - 1,
                      std::ostream_iterator<std::string>(sb, ", "));
            sb << blacklisted_ips.back().c_str();

            FILTERPP->log(LogLevel::INFO, sb.str());
        }
    }
    std::vector <std::string> blocked_ips = FILTERPP->block(FILTERPP->get_blacklist(), ports);
    if (blocked_ips.empty()) {
        FILTERPP->log(LogLevel::INFO, "No IP addresses added to iptables");
    } else {
        std::stringstream ips_added;
        ips_added << "IP addresses added to iptables:\n";

        std::copy(blocked_ips.begin(), blocked_ips.end() - 1,
                  std::ostream_iterator<std::string>(ips_added, ", "));
        ips_added << blocked_ips.back().c_str();

        FILTERPP->log(LogLevel::INFO, ips_added.str());
    }

}


int main() {
    pid_t pid, sid;

    // Fork the current process
    pid = fork();
    // The parent process continues with a process ID greater than 0
    if (pid > 0)
        exit(EXIT_SUCCESS);
        // A process ID lower than 0 indicates a failure in either process
    else if (pid < 0) {
        FILTERPP->log(LogLevel::INFO, "Could not fork the parent process");
        exit(EXIT_FAILURE);
    }
    // The parent process has now terminated, and the forked child process will continue
    // (the pid of the child process was 0)

    // Since the child process is a daemon, the umask needs to be set so files and logs can be written
    umask(0);

    // Open system logs for the child process
    openlog("filterppd", LOG_NOWAIT | LOG_PID, LOG_USER);
    FILTERPP->log(LogLevel::INFO, "Successfully started filterpp");

    // Generate a session ID for the child process
    sid = setsid();
    // Ensure a valid SID for the child process
    if (sid < 0) {
        // Log failure and exit
        FILTERPP->log(LogLevel::ERROR, "Could not generate session ID for child process");

        // If a new session ID could not be generated, we must terminate the child process
        // or it will be orphaned
        exit(EXIT_FAILURE);
    }

    // Change the current working directory to a directory guaranteed to exist
    if ((chdir("/")) < 0) {
        // Log failure and exit
        FILTERPP->log(LogLevel::ERROR, "Could not change working directory to /");

        // If our guaranteed directory does not exist, terminate the child process to ensure
        // the daemon has not been hijacked
        exit(EXIT_FAILURE);
    }

    // A daemon cannot use the terminal, so close standard file descriptors for security reasons
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    // Daemon-specific intialization should go here
    const int SLEEP_INTERVAL = FILTERPP->get_interval();

    FILTERPP->read_options(FILTERPP->get_configuration_file());

    // Enter daemon loop
    while (true) {
        // Execute daemon heartbeat, where your recurring activity occurs
        try {
            do_heartbeat();
        } catch (const invalid_ip_address &e) {
            FILTERPP->log(LogLevel::WARN, e.what());
        } catch (const std::exception &e) {
            FILTERPP->log(LogLevel::ERROR, e.what());
            exit(EXIT_FAILURE);
        }

        // Sleep for a period of time
        sleep(SLEEP_INTERVAL);
    }

    FILTERPP->log(LogLevel::INFO, "Stopping filterpp");

    // Terminate the child process when the daemon completes
    exit(EXIT_SUCCESS);
}