/**
 * FilterPP
 * File: exceptions.cpp
 * Purpose: Provides exception class implementations
 *
 * @author Encryptio
 * @version 0.10
 */

#include "../include/exceptions.h"

port_out_of_range::port_out_of_range(const std::string &context, const unsigned long &given_port)
        : std::invalid_argument(context + ": port given: " + std::to_string(given_port)) {

}

file_open_error::file_open_error(const std::string &context, const std::string &file)
        : std::runtime_error(context + ": file :" + file) {

}

invalid_ip_address::invalid_ip_address(const std::string &context, const std::string &ip_address)
        : std::invalid_argument(context + ": IP address :" + ip_address) {

}

empty_error::empty_error(const std::string &context, const std::string &argument)
        : std::logic_error(context + " argument :" + argument) {

}

match_error::match_error(const std::string &context, const std::string &str)
        : std::runtime_error(context + " string :" + str) {

}
