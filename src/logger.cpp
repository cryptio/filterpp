/**
 * FilterPP
 * File: Logger.cpp
 * Purpose: Implementation of the Logger class
 *
 * @author Encryptio
 * @version 0.10
 */

#include "../include/logger.h"
#include "../include/exceptions.h"
#include <fstream>
#include <chrono>
#include <sstream>

Logger::Logger(const LogLevel &level, std::string log_file)
        : level_(level), log_file_(std::move(log_file)) {
    ;

}

void Logger::log(const LogLevel &level, const std::string &msg) {
    if (level <= level_) {
        std::ofstream os(log_file_.c_str(), std::ios_base::app);
        if (!os)
            throw std::runtime_error("Error opening log file");

        const std::string formatted_msg = "[" + get_formatted_time() + "] " + msg;

        os << formatted_msg << '\n';
    }
}

const LogLevel &Logger::get_log_level() const {
    return level_;
}

void Logger::set_log_level(const LogLevel &level) {
    level_ = level;
}

const std::string &Logger::get_log_file() const {
    return log_file_;
}

std::map<std::string, LogLevel> Logger::get_levels() {
    return log_levels_;
}

void Logger::set_log_file(const std::string &log) {
    std::ifstream is(log.c_str());
    if (!is)
        throw file_open_error("logger::set_log_file", log);

    log_file_ = log;
}

std::string get_formatted_time() {
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    std::time_t tt = std::chrono::system_clock::to_time_t(now);
    tm local_tm = *localtime(&tt);

    std::stringstream ss;
    ss << (local_tm.tm_mon + 1) << '/' << local_tm.tm_mday << '/' << (local_tm.tm_year + 1900)
       << ' ' << local_tm.tm_hour << ':' << local_tm.tm_min << ':' << local_tm.tm_sec;

    return ss.str();

}