/**
 * FilterPP
 * File: abuseipdb.h
 * Purpose: Implements a client library for AbuseIPDB
 *
 * @author Encryptio
 * @version 0.10
 */

#include "../include/abuseipdb.h"
#include <unordered_map>
#include <iostream>

CheckResponse::operator bool() {
    const bool valid = !(
            ip_address.empty() &&
            country_code.empty() &&
            usage_type.empty() &&
            isp.empty() &&
            domain.empty() &&
            last_reported_at.empty()
    );
    return valid;
}

void to_json(json &j, const CheckResponse &response) {
    j = json{
            {"ipAddress",            response.ip_address},
            {"isPublic",             response.is_public},
            {"ipVersion",            response.ip_version},
            {"abuseConfidenceScore", response.abuse_confidence_score},
            {"countryCode",          response.country_code},
            {"usageType",            response.usage_type},
            {"isp",                  response.isp},
            {"domain",               response.domain},
            {"totalReports",         response.total_reports},
            {"lastReportedAt",       response.last_reported_at}
    };
}

void from_json(const json &j, CheckResponse &response) {
    j.at("data").at("ipAddress").get_to(response.ip_address);
    j.at("data").at("isPublic").get_to(response.is_public);
    /**
     * An error is thrown if any of these values are null, so checking has to be done first
     */
    if (j.at("data").at("isWhitelisted") != nullptr) {
        j.at("data").at("isWhitelisted").get_to(response.is_whitelisted);

    }
    j.at("data").at("ipVersion").get_to(response.ip_version);
    j.at("data").at("abuseConfidenceScore").get_to(response.abuse_confidence_score);
    if (j.at("data").at("countryCode") != nullptr) {
        j.at("data").at("countryCode").get_to(response.country_code);
    }
    if (j.at("data").at("usageType") != nullptr) {
        j.at("data").at("usageType").get_to(response.usage_type);

    }
    if (j.at("data").at("isp") != nullptr) {
        j.at("data").at("isp").get_to(response.isp);
    }
    if (j.at("data").at("domain") != nullptr) {
        j.at("data").at("domain").get_to(response.domain);
    }
    j.at("data").at("totalReports").get_to(response.total_reports);
    if (j.at("data").at("lastReportedAt") != nullptr) {
        j.at("data").at("lastReportedAt").get_to(response.last_reported_at);
    }
}

void to_json(json &j, const ErrorResponse &response) {
    const std::unordered_map <std::string, std::vector<Error>> errors = {
            {"errors", response.errors}
    };
    /**
     * {
     *  "errors": [
     *      {
     *          "detail": "...",
     *          "status": 200
     *      }
     *  ]
     * }
     */
    j = json{errors};
}


void from_json(const json &j, ErrorResponse &response) {
    for (const auto &item : j.at("errors")) {
        Error error;
        item.at("detail").get_to(error.detail);
        item.at("status").get_to(error.status);
        response.errors.push_back(error);
    }
}

void to_json(json &j, const Error &error) {
    j = json{
            {"detail", error.detail},
            {"status", error.status}
    };
}

void from_json(const json &j, Error &error) {
    j.at("detail").get_to(error.detail);
    j.at("status").get_to(error.status);
}

AbuseIPDB::AbuseIPDB(std::string api_key) : api_key_(std::move(api_key)), url_("https://api.abuseipdb.com/api/v2") {
    ;
}

CheckResponse AbuseIPDB::check(const std::string &ip_address) {
    CURL *handle = curl_easy_init();
    CURLcode code;
    std::string response;

    std::ostringstream ss;
    ss << url_ << "/check?" << "ipAddress=" << ip_address;

    if (!handle) {
        std::stringstream curl_err;
        curl_err << "Invalid curl handle: " << curl_easy_strerror(code);
        throw std::runtime_error(curl_err.str());
    }

    struct curl_slist *chunk = nullptr;
    const std::string key_header = "Key: " + api_key_;
    chunk = curl_slist_append(chunk, key_header.c_str());
    chunk = curl_slist_append(chunk, "Accept: application/json");


    code = curl_easy_setopt(handle, CURLOPT_HTTPHEADER, chunk);

    curl_easy_setopt(handle, CURLOPT_URL, ss.str().c_str());
    curl_easy_setopt(handle, CURLOPT_HTTPHEADER, chunk);
    curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, details::write_callback);   // string conversion
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, &response);
    code = curl_easy_perform(handle);
    curl_easy_cleanup(handle);

    /*
     * If we don't get a 200 (OK) HTTP response code we will output an error, but it is not necessary to
     * throw an error and cause complete program failure.
     */
    if (code == CURLE_OK) {
        const auto j = json::parse(response);
        if (j.find("errors") != j.end()) {
            ErrorResponse errorResponse = j.get<ErrorResponse>();
            errors_ = errorResponse;
        } else if (j.find("data") != j.end()) {
            CheckResponse checkResponse = j.get<CheckResponse>();
            return checkResponse;
        } else {
            return CheckResponse();
        }
    } else {
        std::stringstream curl_err;
        curl_err << "curl_easy_perform() failed: " << curl_easy_strerror(code);
        throw std::runtime_error(curl_err.str());
    }

    return CheckResponse();
}

ErrorResponse AbuseIPDB::get_errors() const {
    return errors_;
}

size_t details::write_callback(void *contents, size_t size, size_t nmemb, void *userp) {
    ((std::string *) userp)->append((char *) contents, size * nmemb);
    return size * nmemb;
}