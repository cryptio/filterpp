/**
 * FilterPP
 * File: protocol.cpp
 * Purpose: Implementation of the Protocol class
 *
 * @author Encryptio
 * @version 0.10
 */

#include "../include/protocol.h"
#include "../include/exceptions.h"
#include <fstream>


Protocol::Protocol(
        std::string name,
        std::string log_file,
        const std::string &expression,
        const unsigned long &port
) : name_(std::move(name)), log_file_(std::move(log_file)), expression_(expression), port_(port) {
    const bool name_is_empty = name.empty();
    const bool expression_is_empty = expression.empty();
    const bool port_is_valid = (port > 0) && (port < 65535);

    if (name_is_empty)
        throw empty_error("Protocol::protocol", "name");

    if (expression_is_empty)
        throw empty_error("Protocol::protocol", "expression");

    if (!port_is_valid)
        throw port_out_of_range("Protocol::protocol", port);
}

const std::string &Protocol::get_name() const {
    return name_;
}

const std::string &Protocol::get_log_file() const {
    return log_file_;
}

const std::regex &Protocol::get_expression() const {
    return expression_;
}

const unsigned long &Protocol::get_port() const {
    return port_;
}

void Protocol::set_name(const std::string &name) {
    if (name.empty())
        throw empty_error("Protocol::set_name", "name");
    else
        name_ = name;
}

void Protocol::set_log_file(const std::string &log_file) {
    std::ifstream is(log_file.c_str());

    if (is)
        log_file_ = log_file;
    else
        throw file_open_error("Protocol::set_log_file", log_file);
}

void Protocol::set_expression(const std::regex &expression) {
    expression_ = expression;
}

void Protocol::set_port(const unsigned long &port) {
    const bool port_is_valid = (port > 0) && (port < 65535);

    if (port_is_valid)
        port_ = port;
    else
        throw port_out_of_range("Protocol::set_port", port);
}