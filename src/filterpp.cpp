/**
 * FilterPP
 * File: filterpp.cpp
 * Purpose: Implementation of the FilterPP class
 *
 * @author Encryptio
 * @version 0.14
 */

#include "../include/filterpp.h"
#include "../include/exceptions.h"
#include "../lib/simpleini/SimpleIni.h"
#include "../include/abuseipdb.h"
#include <fstream>
#include <curl/curl.h>


std::map <std::string, LogLevel> Logger::log_levels_ = {
        {"ERROR", LogLevel::ERROR},
        {"WARN",  LogLevel::WARN},
        {"INFO",  LogLevel::INFO},
        {"DEBUG", LogLevel::DEBUG}
};

FilterPP::FilterPP()
        : logger_(LogLevel::WARN, "/var/log/filterpp.log") {
    ;
}

// Get the number of fails for each IP
std::unordered_map<std::string, int> FilterPP::get_fails(
        const std::string &input_file,
        const std::regex &expression) {
    std::unordered_map<std::string, int> ip_addresses;

    std::ifstream is(input_file.c_str());
    if (!is) {
        throw file_open_error("FilterPP::getfails", input_file);
    }

    smatch matches;
    std::string buffer;
    while (std::getline(is, buffer)) {
        if (std::regex_search(buffer, matches, expression)) {
            if (std::find(whitelist_.begin(), whitelist_.end(), matches[1]) ==
                whitelist_.end()) { // ignore whitelisted IP addresses
                ++ip_addresses[matches[1]];  // increment the number of attempts for the IP
            }
        }
    }

    return ip_addresses;
}

// Check the number of reports for a given IP address
unsigned FilterPP::check_reports(const std::string &ip_address) {
    const std::string aipdb_key = "2de733ed3c3231ad969c485e332e38747eb6a16c8cfb84302f74f3fc56067c2abe3cff7c8e448b6d";
    AbuseIPDB abuseIPDB(aipdb_key);
    CheckResponse response = abuseIPDB.check(ip_address);
    if (!response) {
        std::string errors_string = "AbuseIPDB errors:\n";
        for (const auto &errors: abuseIPDB.get_errors().errors) {
            errors_string += "Status:" + std::to_string(errors.status) + "\nMessage:" + errors.detail;
        }
        log(LogLevel::WARN, errors_string);
        return 0;
    }
    return response.total_reports;
}

// Write the IPs to the blacklist
std::vector <std::string> FilterPP::write_blacklist(
        std::unordered_map<std::string, int> &ip_addresses,
        const std::string &output_file) {
    std::ifstream is(output_file.c_str());
    std::vector <std::string> blacklisted_ips;

    if (!is)
        throw file_open_error("FilterPP::write_blacklist", output_file);

    if (ip_addresses.empty()) // no work to do, return early
        return blacklisted_ips;

    // ignore duplicates
    std::string buf;
    while (std::getline(is, buf)) {
        if (ip_addresses.find(buf) != ip_addresses.end()) {
            ip_addresses.erase(buf);
            const std::string msg = buf + " already in blacklist, skipping...";
            log(LogLevel::DEBUG, msg);
        }
    }

    // delete the IPs that don't meet the criteria
    auto a = ip_addresses.begin();

    while (a != ip_addresses.end()) {
        bool criteria_met = false;

        // Not enough attempts
        if (a->second < max_attempts_) {
            /* An iterator to the following element ahead of the one that was just erased is returned, no need to
               manually move the iterator ahead with ++
            */
            a = ip_addresses.erase(a);
            criteria_met = true;
        } else {
            // Exceeded the max attempts, add it
            if (a->second > max_attempts_) {
                // use database checking, the IP must exceed the max reports
                if (enable_database_checking_) {
                    const unsigned number_of_reports = check_reports(a->first);
                    if (number_of_reports < max_reports_) {
                        a = ip_addresses.erase(a);
                        criteria_met = true;
                    }
                }
            }
        }

        /* Instead of handling all of the edge cases with else statements for every if and then incrementing, just keep
         * track of whether or not we deleted an element on this iteration. If we didn't move to the next element
         */
        if (!criteria_met)
            ++a;
    }

    // write the remaining IPs to the blacklist
    std::ofstream os(output_file.c_str(), std::ios_base::app);

    for (const auto &[ip_address, number_of_reports]: ip_addresses) {
        const bool exceeds_max_attempts = number_of_reports > max_attempts_;

        if (!exceeds_max_attempts)   // skip
            continue;

        os << ip_address << '\n';
        blacklisted_ips.push_back(ip_address);

        std::stringstream database_checking_msg;

        database_checking_msg << ip_address.c_str() << " had " << number_of_reports << " failed attempts";
        if (enable_database_checking_)
            database_checking_msg << " and " << check_reports(ip_address) << " abuse reports";
        database_checking_msg << ", adding to blacklist...";

        log(LogLevel::INFO, database_checking_msg.str());
    }

    return blacklisted_ips;
}

// Block the IP addresses
std::vector <std::string> FilterPP::block(
        const std::string &blacklist,
        const std::vector<unsigned long> &ports) {


    std::ifstream is(blacklist_.c_str());
    if (!is)
        throw file_open_error("FilterPP::block", blacklist_);

    // compare IPs in the blacklist against the rules currently in iptables, we don't want duplication
    std::string buffer;
    const std::string current_rules = details::run("iptables-save");
    std::vector <std::string> blocked_ips;

    while (std::getline(is, buffer)) {
        std::stringstream iptables_str;
        iptables_str << "-A INPUT -s " << buffer << "\\/\\d{2} -p tcp -m multiport --dports ";

        // an elegant way of outputting comma separated elements minus the last element with no iteration
        std::copy(ports.begin(), ports.end() - 1, std::ostream_iterator<unsigned long>(iptables_str, ","));
        iptables_str << ports.back() << " -j DROP";
        std::regex iptables_pattern(iptables_str.str());

        if (buffer.find_last_not_of(' ') == std::string::npos) {  // ignore blank lines
            ;
        } else if (std::regex_search(current_rules, iptables_pattern)) {
            std::string msg = buffer + " already in iptables, skipping...";
            log(LogLevel::DEBUG, msg);
        } else {
            // add the rule in iptables
            std::stringstream ss;
            ss << "/sbin/iptables -A INPUT -s \"" << buffer << "\" -p tcp -m multiport --dports ";

            std::copy(ports.begin(), ports.end() - 1, std::ostream_iterator<unsigned long>(ss, ","));
            ss << ports.back() << " -j DROP";
            details::run(ss.str());

            std::string msg = "Added " + buffer + " to iptables";
            log(LogLevel::INFO, msg);
            if (std::find(blocked_ips.begin(), blocked_ips.end(), buffer) == blocked_ips.end())
                blocked_ips.push_back(buffer);
        }
    }
    return blocked_ips;
}

// Read the configuration options
void FilterPP::read_options(const std::string &configuration_file) {
    CSimpleIniA ini;
    ini.SetUnicode();
    ini.SetMultiKey();
    SI_Error conf_file = ini.LoadFile(configuration_file.c_str());
    if (conf_file < 0)
        throw file_open_error("FilterPP::read_options", configuration_file);

    CSimpleIniA::TNamesDepend sections;
    ini.GetAllSections(sections);

    for (const auto &section : sections) {
        std::string section_name(section.pItem);
        if (section_name == "general") {
            CSimpleIniA::TNamesDepend values;
            ini.GetAllValues("general", "whitelist", values);
            values.sort(CSimpleIniA::Entry::LoadOrder());

            std::regex ipv4("((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)");
            for (const auto &value : values) {
                if (std::regex_match(value.pItem, ipv4))
                    whitelist_.emplace_back(value.pItem);
                else
                    throw invalid_ip_address("FilterPP::read_options", value.pItem);
            }

            const char *blacklist = ini.GetValue("general", "blacklist_file", "/etc/filterpp/blacklist.lst");
            const char *output_log = ini.GetValue("general", "output_log", "/var/log/filterpp.log");
            const char *max_t = ini.GetValue("general", "max_tries", "5");
            const char *max_r = ini.GetValue("general", "max_reports", "2");
            const unsigned casted_max_t = std::stoi(max_t);
            const unsigned casted_max_r = std::stoi(max_r);
            const char *verbosity_level = ini.GetValue("general", "verbosity", "WARN");
            const unsigned enable_database_checking = std::stoi(
                    ini.GetValue("general", "enable_database_checking", "1")
            );


            blacklist_ = blacklist;

            logger_.set_log_file(output_log);
            std::string log_msg = "Logging to ";
            log_msg += output_log;
            log(LogLevel::INFO, log_msg);

            const bool invalid_max_values = (casted_max_t < 1) && (casted_max_r < 1);
            if (invalid_max_values) {
                throw std::logic_error("One or more invalid max values in general configuration");
            } else {
                max_attempts_ = casted_max_t;
                max_reports_ = casted_max_r;
            }

            if (Logger::get_levels().find(verbosity_level) != Logger::get_levels().end())
                logger_.set_log_level(Logger::get_levels()[verbosity_level]);

            switch (enable_database_checking) {
                case 1:
                    enable_database_checking_ = true;
                    log(LogLevel::DEBUG, "enable_database_checking is enabled");
                    break;
                case 0:
                    enable_database_checking_ = false;
                    log(LogLevel::DEBUG, "enable_database_checking is disabled");
                    break;
                default:
                    throw std::invalid_argument("Invalid value for enable_database_checking");
            }

            log(LogLevel::INFO, "Loaded general settings");
        } else {    // Not the general section, so it's a custom protocol
            const unsigned long port = std::stoi(ini.GetValue(section_name.c_str(), "port", "0"));
            const std::string expression = ini.GetValue(section_name.c_str(), "expression", "");

            const std::string protocol_log_file = ini.GetValue(section_name.c_str(), "protocol_log", "");
            const Protocol custom(section_name, protocol_log_file, expression, port);
            protocols_.push_back(custom);

            const std::string msg = "Loaded service \"" + section_name + "\"";
            log(LogLevel::INFO, msg);
        }
    }
}

// Wrapper for the logger's log() function
void FilterPP::log(const LogLevel &level, const std::string &msg) {
    logger_.log(level, msg);
}

const LogLevel &FilterPP::get_verbosity_level() const {
    return logger_.get_log_level();
}

const unsigned &FilterPP::get_interval() const {
    return interval_;
}

const unsigned &FilterPP::get_max_attempts() const {
    return max_attempts_;
}

const unsigned &FilterPP::get_max_reports() const {
    return max_reports_;
}

const std::vector <Protocol> &FilterPP::get_protocols() const {
    return protocols_;
}

const std::string &FilterPP::get_blacklist() const {
    return blacklist_;
}

const std::string &FilterPP::get_configuration_file() const {
    return configuration_file_;
}

std::string details::run(const std::string &cmd) {
    std::array<char, 128> buffer;
    std::string result; // the result
    std::shared_ptr <FILE> pipe(popen(cmd.c_str(), "r"), pclose);
    if (!pipe)
        throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
            result += buffer.data();
    }
    return result;
}