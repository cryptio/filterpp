#!/usr/bin/env bash

if [[ "$EUID" -ne 0 ]]
then
    echo "Run it as root"
    exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

check_dependencies() {
    # ensure that the dependencies are installed
    local packages=(cmake make)
    for package in "${packages[@]}"
    do
        local package_version=$(${package} --version)
        local package_installed=$?
        if [[ ! ${package_installed} -eq 0 ]]
        then
	        echo "$package was not found"
	        exit 1
	    fi
    done

    # check for the libcurl library
    local libcurl_installed=$(ldconfig -p | grep libcurl)
    if [[ -z "$libcurl_installed" ]]
    then
        echo "libcurl was not found"
	    exit 1
    fi
}

create_files() {
    echo "Where do you want filterpp to write its IP addresses to? (leave empty for /etc/filterpp/blacklist.lst)"
    local blacklist_file
    read blacklist_file

    # input was empty, defaulting
    if [[ -z ${blacklist_file} ]]
    then
        blacklist_file="/etc/filterpp/blacklist.lst"
    fi

    echo "Modifying filterpp.conf"
    sed -i '/blacklist_file/c\blacklist_file = '"$blacklist_file" "$DIR"/filterpp.conf  # update the value of blacklist_file

    if [[ -f ${blacklist_file} ]]
    then
        echo "File $blacklist_file already exists"
    else
        local blacklist_dir=${blacklist_file%/*}  # check if blacklist's parent directory exists
        if [[ -d ${blacklist_dir} ]]
        then
            echo ${blacklist_dir} " already exists"
            else
                echo ${blacklist_dir} " does not exist, creating..."
            mkdir ${blacklist_dir}
        fi

        echo "Creating blacklist file..."
            touch ${blacklist_file}  # create the blacklist file
    fi

    echo "Where do you want filterpp to log to? (leave empty for /var/log/filterpp.log)"
    local log_file
    read log_file

    # input was empty, defaulting
    if [[ -z ${log_file} ]]
    then
        log_file="/var/log/filterpp.log"
    fi

    if [[ -f ${log_file} ]]
    then
        echo "File $log_file already exists"
    else
        log_dir=${log_file%/*}  # check if log file's parent directory exists
        if [[ -d ${log_dir} ]]
        then
            echo ${log_dir} " already exists"
            else
                echo ${log_dir} " does not exist, creating..."
            mkdir ${log_dir}
         fi

            echo "Creating $log_file..."
            touch ${log_file}
    fi

    echo "Modifying filterpp.conf"
    sed -i '/output_log/c\output_log = '"$log_file" "$DIR"/filterpp.conf    # update the value of output_log

    if [[ -d "/etc/filterpp/filterpp" ]]
    then
        echo "/etc/filterpp/filterpp already exists"
    else
        echo "/etc/filterpp/filterpp does not exist, creating..."
        mkdir "/etc/filterpp/filterpp"
    fi

    # put the newly modified configuration file in a place where the program can read it
    cp "$DIR"/filterpp.conf /etc/filterpp
}

compile_filterpp() {
    cmake "$DIR"/CMakeLists.txt
    make
    chmod +x "$DIR"/filterpp
}

daemonize_filterpp() {
    local daemonize
    while true; do
        read -p "Do you want to install filterpp as a systemd service? " daemonize
        case $daemonize in
            [Yy]* )
                cp "$DIR"/filterpp /usr/sbin
                cp "$DIR"/src/filterppd.sh /etc/init.d/filterpp
                chmod +x /etc/init.d/filterpp
                systemctl enable filterpp
             break;;
            [Nn]* ) exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}

main() {
    check_dependencies
    create_files
    compile_filterpp
    daemonize_filterpp
    local green='\033[0;32m'
    local no_color='\033[0m'
    echo -e "${green}filterpp has been successfully installed!${no_color}"

}

main
