# filterpp
A tool for preventing bruteforce-based attacks against remote logins by scanning log files, determining which hosts are malicious, and blocking them.
Features:
- Log auditing
- Malicious IP address filtering
- Abuse database query
- Support for multiple services
- Support for custom services
- Advanced configuration options

## To Do
- [ ] IPv6 support
- [ ] threading


## Requirements
The prequisites for this project are the cURL library (most if not all distros will have a libcurl package) and [nlohmann/json](https://github.com/nlohmann/json). `cmake` and C++14 support is also needed to build the source code.

## Installation
To install `filterpp`, simply give `install.sh` executable permissions and run it. The necessary paths will be created using the installer. If you chose to install `filterpp` as a `systemd` service, it can run using either `service filterpp start` or `systemctl start filterpp`(as root). Otherwise, you will have to handle its management yourself, either by using `nohup` or another method.