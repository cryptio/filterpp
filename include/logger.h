/**
 * FilterPP
 * File: logger.h
 * Purpose: Interface for the Logger class
 *
 * @author Encryptio
 * @version 0.10
 */

#ifndef FILTERPP_LOGGER_H
#define FILTERPP_LOGGER_H

#include <string>
#include <map>
#include <exception>


std::string get_formatted_time();

enum class LogLevel {
    ERROR,
    WARN,
    INFO,
    DEBUG
};

class Logger {
public:
    Logger(const LogLevel &level, std::string log_file);

    /**
     * Writes the message to the file if level_ is equal to or higher than the message's LogLevel
     * @param level The LogLevel for the message
     * @param msg The message to write
     */
    void log(const LogLevel &level, const std::string &msg);

    /**
     * Protected access to get the log level
     * @return level_
     */
    const LogLevel &get_log_level() const;

    /**
     * Setter for the log level
     * @param level The LogLevel to set it to
     */
    void set_log_level(const LogLevel &level);

    /**
     * Protected access to get the log file
     * @return log_file_
     */
    const std::string &get_log_file() const;

    /**
     *
     * @param log
     */
    void set_log_file(const std::string &log);

    /**
     * Get a map of the string representations for the LogLevels
     * @return Map of each LogLevel's corresponding string
     */
    static std::map<std::string, LogLevel> get_levels();

private:
    LogLevel level_;
    std::string log_file_;
    static std::map<std::string, LogLevel> log_levels_;
};


#endif //FILTERPP_LOGGER_H
