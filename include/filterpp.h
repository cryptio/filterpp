/**
 * FilterPP
 * File: filterpp.h
 * Purpose: Interface for the filterpp class
 *
 * @author Encryptio
 * @version 0.14
 */

#ifndef FILTERPP_FILTERPP_H
#define FILTERPP_FILTERPP_H

#include <string>
#include <unordered_map>
#include <regex>
#include <vector>
#include "protocol.h"
#include "logger.h"

typedef std::match_results <std::string::const_iterator> smatch;

class FilterPP {
public:
    static FilterPP &get_instance() {
        static FilterPP instance; // Guaranteed to be destroyed.
        // Instantiated on first use.
        return instance;
    }

    FilterPP(FilterPP const &) = delete;

    void operator=(FilterPP const &) = delete;


    /**
     * Retrieves fails from the log file
     * @param input_file The file to read from
     * @param expr The expression for matching fails
     * @return A map containing the IP addresses and their fails
     */
    std::unordered_map<std::string, int> get_fails(const std::string &input_file, const std::regex &expr);

    /**
     * Checks the number of reports against the specified IP address
     * @param ip_address The IP address to check
     * @return The number of reports
     */
    unsigned check_reports(const std::string &ip_address);

    /**
     * Writes the captured IPs to a blacklist
     * @param ip_addresses A map of IP addresses and their fails
     * @param output_file The blacklist file to write the IPs to
     * @return A vector of the IP addresses written to the file
     */
    std::vector <std::string> write_blacklist(
            std::unordered_map<std::string, int> &ip_addresses,
            const std::string &output_file);

    /**
     * Blocks the IP addresses using iptables
     * @param blacklist The blacklist of IP addresses to read from
     * @param ports The ports to block each IP address from
     * @return The IP addresses that were blocked
     */
    std::vector <std::string> block(const std::string &blacklist, const std::vector<unsigned long> &ports);

    /**
     * Read options from the specified configuration file
     * @param configuration_file The configuration file to read from
     */
    void read_options(const std::string &configuration_file);

    /**
     * A wrapper for the Logger class
     * @param level The message's LogLevel
     * @param msg The message to log
     */
    void log(const LogLevel &level, const std::string &msg);

    /**
     * Get the current verbosity level
     * @return level_
     */
    const LogLevel &get_verbosity_level() const;

    /**
     * Get the interval
     * @return interval_
     */
    const unsigned &get_interval() const;

    /**
     * Get the current number of max attempts
     * @return max_attempts_
     */
    const unsigned &get_max_attempts() const;

    /**
     * Get the current number of max reports
     * @return max_reports_
     */
    const unsigned &get_max_reports() const;

    /**
     * Get the current vector of Protocols
     * @return protocols_
     */
    const std::vector <Protocol> &get_protocols() const;

    /**
     * Get the blacklist file
     * @return blacklist_
     */
    const std::string &get_blacklist() const;

    /**
     * Get the configuration file
     * @return configuration_file_
     */
    const std::string &get_configuration_file() const;

private:
    FilterPP();

    std::string configuration_file_ = "/etc/filterpp/filterpp.conf";
    std::string blacklist_ = "/etc/filterpp/blacklist.lst";
    Logger logger_;
    std::vector <std::string> whitelist_;
    bool enable_database_checking_ = true;
    unsigned max_attempts_ = 5;
    unsigned max_reports_ = 2;
    unsigned interval_ = 15;
    std::vector <Protocol> protocols_;
};

namespace details {
    std::string run(const std::string &cmd);
}


#endif //FILTERPP_FILTERPP_H
