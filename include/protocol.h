/**
 * FilterPP
 * File: protocol.h
 * Purpose: Interface for the Protocol class
 *
 * @author Encryptio
 * @version 0.10
 */

#ifndef FILTERPP_PROTOCOL_H
#define FILTERPP_PROTOCOL_H

#include <string>
#include <regex>


class Protocol {
public:
    Protocol(
            std::string name,
            std::string log_file,
            const std::string &expression,
            const unsigned long &port);

    /**
     * Get the Protocol's name
     * @return name_
     */
    const std::string &get_name() const;

    /**
     * Get the Protocol's log file
     * @return log_file_
     */
    const std::string &get_log_file() const;

    /**
     * Get the Protocol's regular expression
     * @return expression_
     */
    const std::regex &get_expression() const;

    /**
     * Get the Protocol's port number
     * @return port_
     */
    const unsigned long &get_port() const;

    /**
     * Protected access for setting the Protocol's name
     * @param name The name to set the Protocol to
     */
    void set_name(const std::string &name);

    /**
     * Protected access for setting the Protocol's log file
     * @param log_file The Protocol's log file
     */
    void set_log_file(const std::string &log_file);

    /**
     * Sets the Protocol's regular expression for getting fails in its log file
     * @param expression The Protocol's regular expression
     */
    void set_expression(const std::regex &expression);

    /**
     * Sets the Protocol's port
     * @param port The port for the Protcol
     */
    void set_port(const unsigned long &port);

private:
    std::string name_;
    std::string log_file_;
    std::regex expression_;
    unsigned long port_;
};


#endif //FILTERPP_PROTOCOL_H
