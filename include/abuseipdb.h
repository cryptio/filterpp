/**
 * FilterPP
 * File: abuseipdb.h
 * Purpose: Implements a client library for AbuseIPDB
 *
 * @author Encryptio
 * @version 0.10
 */

#ifndef FILTERPP_ABUSEIPDB_H
#define FILTERPP_ABUSEIPDB_H

#include <string>
#include <vector>
#include <nlohmann/json.hpp>
#include <curl/curl.h>
#include <sstream>

using json = nlohmann::json;

struct Report {
    std::string reported_at;
    std::string comment;
    std::vector<int> categories;
    int reporter_id;
    std::string reporter_country_code;
    std::string reporter_country_name;
};

struct Error {
    std::string detail;
    int status;
};

struct ErrorResponse {
    std::vector <Error> errors;
};


// Represents a response from the "check" endpoint
struct CheckResponse {
    std::string ip_address;
    bool is_public;
    int ip_version;
    bool is_whitelisted;
    int abuse_confidence_score;
    std::string country_code;
    std::string usage_type;
    std::string isp;
    std::string domain;
    int total_reports;
    std::string last_reported_at;
    // std::vector<Report> reports;

    explicit operator bool();

};

void to_json(json &j, const CheckResponse &response);

void from_json(const json &j, CheckResponse &response);

void to_json(json &j, const ErrorResponse &response);

void from_json(const json &j, ErrorResponse &response);

void to_json(json &j, const Error &error);

void from_json(const json &j, Error &error);

class AbuseIPDB {
public:
    explicit AbuseIPDB(std::string api_key);

    /**
     * Check the AbuseIPDB website for abuse information
     * @param ip_address The IP address to query
     * @return A response object containing the deserialized JSON information
     */
    CheckResponse check(const std::string &ip_address);

    /**
     * Return the last errors
     * @return Last errors reported
     */
    ErrorResponse get_errors() const;

private:
    std::string api_key_;
    std::string url_;
    ErrorResponse errors_;

};

namespace details {
    static size_t write_callback(void *contents, size_t size, size_t nmemb, void *userp);
}

#endif //FILTERPP_ABUSEIPDB_H
