/**
 * FilterPP
 * File: exceptions.h
 * Purpose: Provides exception class declarations
 *
 * @author Encryptio
 * @version 0.10
 */

#ifndef FILTERPP_EXCEPTIONS_H
#define FILTERPP_EXCEPTIONS_H

#include <iostream>
#include <exception>


struct port_out_of_range : public std::invalid_argument {
    port_out_of_range(const std::string &context, const unsigned long &given_port);
};

struct file_open_error : public std::runtime_error {
    file_open_error(const std::string &context, const std::string &file);
};

struct invalid_ip_address : public std::invalid_argument {
    invalid_ip_address(const std::string &context, const std::string &ip_address);
};

struct empty_error : public std::logic_error {
    empty_error(const std::string &context, const std::string &argument);
};

struct match_error : public std::runtime_error {
    match_error(const std::string &context, const std::string &str);
};

#endif //FILTERPP_EXCEPTIONS_H
